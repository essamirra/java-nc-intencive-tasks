import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Proinina Maria
 */
public class GenericUtilsTest {
    @org.junit.Test
    public void greaterThenValueIntTest() throws Exception {
        List<Integer> src = new LinkedList<>();
        Collections.addAll(src, 12, 14, 15, 1, 35);
        List<Integer> result = GenericUtils.GreaterThenValue(src, 14);
        List<Integer> expectedResult = new LinkedList<>();
        Collections.addAll(expectedResult,  15, 35);
        assertEquals(result, expectedResult);
    }
    @org.junit.Test
    public void greaterThenValueDoubleTest() throws Exception {
        List<Double> src = new LinkedList<>();
        Collections.addAll(src, 12.0, 14.0, 15.0, 1.0, 1.0, 35.0);
        List<Double> result = GenericUtils.GreaterThenValue(src, 1.0);
        List<Double> expectedResult = new LinkedList<>();
        Collections.addAll(expectedResult,  12.0, 14.0, 15.0, 35.0);
        assertEquals(result, expectedResult);
    }

}