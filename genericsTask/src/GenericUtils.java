import java.util.LinkedList;
import java.util.List;

/**
 * Created by Proinina Maria
 */
public class GenericUtils {
    public static <T extends Number & Comparable<T>> List<T> GreaterThenValue(List<T> source, T value){
        List<T> result = new LinkedList<T>();
        for (T num:source) {
            if(num.compareTo(value) == 1)
                result.add(num);

        }
        return result;
        //return source.stream().filter(num -> num.compareTo(value) == 1).collect(Collectors.toCollection(LinkedList::new));
    }
}
