import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Proinina Maria
 */
class Node {
    Long id;
    List<Node> children;
}

public class Tree{

    public void remove(Node root, Long id) {
        if (root == null)
            throw new RuntimeException("Tree is empty");
        if(root.children == null && root.id == id) {
            throw new RuntimeException("Can not delete root");
        }

        Queue<Pair> path = root.children.stream()
                .map(node -> new Pair(root, node))
                .collect(Collectors.toCollection(LinkedList::new));
       while (!path.isEmpty())
       {
           Pair current = path.remove();
           if (current.child == null)
               continue;
           if(Objects.equals(current.child.id, id))
           {
               current.parent.children.remove(current.child);
               return;
           }
           else {
               path.addAll(current.child.children.stream()
                       .map(node -> new Pair(current.child, node))
                       .collect(Collectors.toList()));
           }
       }
       throw new RuntimeException("No such id in tree");
    }
    public Node search(Node root, long id) {
        if (root == null)
            throw new RuntimeException("Tree is empty");
        Queue<Node> path =  new LinkedList<>(root.children);
        while (!path.isEmpty())
        {
            Node current = path.remove();
            if (current == null)
                continue;
            if(Objects.equals(current.id, id))
            {
                return current;
            }
            else {
                path.addAll(current.children);
            }
        }
        return null;
    }

    private class Pair {
        Node parent;
        Node child;

        Pair(Node parent, Node child) {
            this.parent = parent;
            this.child = child;
        }
    }
}
