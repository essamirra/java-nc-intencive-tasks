import java.util.*;

/**
 * Created by Proinina Maria
 */
public class CollectionUtils {
    public static <T> LinkedList<T> RemoveDuplicates(List<T> src)
    {
        if(src == null) return null;
        if (src.isEmpty()) return new LinkedList<T>();
        Set<T> result = new LinkedHashSet<T>(src);
        return new LinkedList<T>(result);
    }
}
