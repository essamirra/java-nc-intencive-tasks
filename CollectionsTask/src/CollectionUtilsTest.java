import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Proinina Maria
 */
public class CollectionUtilsTest {
    @org.junit.Test
    public void removeDuplicatesWorksWithNull() throws Exception {
        List<Object> src = new LinkedList<>();
        Collections.addAll(src, null, null, null);
        List<Object> excpected = new LinkedList<>();
        excpected.add(null);
        assertEquals(excpected, CollectionUtils.RemoveDuplicates(src));

    }

    @org.junit.Test
    public void removeDuplicatesWorksNullSource() throws Exception {
        List<Object> src = null;
        List<Object> excpected = null;
        assertEquals(excpected, CollectionUtils.RemoveDuplicates(src));

    }

    @org.junit.Test
    public void removeDuplicatesWorksWithNumbers() throws Exception {
        List<Object> src = new LinkedList<>();
        Collections.addAll(src, 5, 5, 4);
        List<Object> excpected = new LinkedList<>();
        Collections.addAll(excpected, 5, 4);
        assertEquals(excpected, CollectionUtils.RemoveDuplicates(src));

    }


}