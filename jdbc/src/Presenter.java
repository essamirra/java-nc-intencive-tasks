import oracle.jdbc.proxy.annotation.Pre;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Proinina Maria
 */


public class Presenter {
    IMenu view;
    DataManager dm;
    int currentOption = -1;
    private final static String INITIAL_MESSAGE = "Scott-tiger database application. Choose option from menu below";
    private final static String DRIVER_ERROR = "Something is wrong with application files. Please, download them from repository again.";
    private final static String DATABASE_ERROR = "Can not connect to database. Please, check your internet connection and try again";
    private final static String FIND_EMPLOYEE_MESSAGE = "Find employee by his/her id";
    private final static String ADD_EMPLOYEE_MESSAGE = "Add new employee to database";
    private final static String DELETE_EMPLOYEE_MESSAGE = "Delete employee from database";
    private final static String FIND_EMPLOYEE_BY_JOB_MESSAGE = "Find employee by job";
    private final static String FIND_EMPLOYEE_BY_DEPT_MESSAGE = "Find employee by department";
    private final static String INDEX_ERROR_MESSAGE = "Incorrect option, input index of option again";
    private final static String NO_RESULT_MESSAGE = "No entries found";
    private final static String INPUT_OPERATION_MESSAGE = "Input operation code or \"exit\" to exit application";
    public final static String EXIT_STRING = "exit";
    private static Map<Integer, String> menuStrings = new HashMap<>();
    private static Map<Integer, String> optionsDetails = new HashMap<>();

    static {
        menuStrings.put(1, FIND_EMPLOYEE_MESSAGE);
        menuStrings.put(2, ADD_EMPLOYEE_MESSAGE);
        menuStrings.put(3, DELETE_EMPLOYEE_MESSAGE);
        menuStrings.put(4, FIND_EMPLOYEE_BY_JOB_MESSAGE);
        menuStrings.put(5, FIND_EMPLOYEE_BY_DEPT_MESSAGE);
        optionsDetails.put(1, "Input employee id");
        optionsDetails.put(2, "Fill all inputs, for exit type \"exit\"");
        optionsDetails.put(3, "Input id of employee to delete his record");
        optionsDetails.put(4, "Input job name");
        optionsDetails.put(5, "Input department id");
    }

    public Presenter(IMenu view) {
        this.view = view;
        connectToDatabase(view);
    }

    public void onMenuLoad() {
        initMenu();
    }

    private void initMenu() {
        view.showMenu(menuStrings);
        view.showMessage(INPUT_OPERATION_MESSAGE);
        view.showOperationInput();
    }

    public void onOperationInput(int index) {
        currentOption = index;
        if (index < 1 && index > 5) {
            view.showMessage(INDEX_ERROR_MESSAGE);
            view.showOperationInput();
        } else {
            view.showMessage(optionsDetails.get(index));
            switch (currentOption) {
                case 1:
                    view.showIntParameterInput();
                    break;
                case 2:
                    view.showEmployeeInput();
                    break;
                case 3:
                    view.showIntParameterInput();
                    break;
                case 4:
                    view.showStringParameterInput();
                    break;
                case 5:
                    view.showIntParameterInput();
                    break;
            }
        }
    }

    public void onStringParameterInput(String input) {
        switch (currentOption) {
            case 4:
                try {
                    List<Employee> result = dm.findEmploeeByName(input);
                    for (Employee e : result) {
                        view.showMessage(e.toString());
                    }

                } catch (SQLException e) {
                    view.showMessage(NO_RESULT_MESSAGE);
                }
                break;

        }
        initMenu();
    }

    public void onIntParameterInput(int input) {
        switch (currentOption) {
            case 1:
                try {
                    Employee e = dm.findEmploee(input);
                    view.showMessage(e.toString());


                } catch (SQLException e) {
                    view.showMessage(NO_RESULT_MESSAGE);
                }
                break;
            case 3:
                try {
                    dm.deleteEmploee(input);
                    view.showMessage("Employee has been deleted");

                } catch (SQLException e) {
                    view.showMessage("Could not delete employee - check your input and try again");

                }
                break;
            case 5: {
                try {
                    List<Employee> result = dm.findEmploeeByDept(input);
                    for (Employee e : result) {
                        view.showMessage(e.toString());
                    }

                } catch (SQLException e) {
                    view.showMessage(NO_RESULT_MESSAGE);
                }

            }

        }
        initMenu();
    }


    public void onEmployeeInput(Employee e) {
        try {
            dm.insertEmployee(e);
            view.showMessage("Employee has been added");
            initMenu();
        } catch (SQLException e1) {
            view.showMessage("Can not insert employee, check your input and try again");
            initMenu();
        }
    }

    private void connectToDatabase(IMenu view) {
        view.showMessage(INITIAL_MESSAGE);
        dm = new DataManager();
        try {
            dm.connectToDatabase();
        } catch (ClassNotFoundException e) {
            view.showMessage(DRIVER_ERROR);
        } catch (SQLException e) {
            view.showMessage(DATABASE_ERROR);
        }
    }

    public void exit() {
        try {
            dm.closeConnection();
        } catch (SQLException e) {
            view.showMessage("Database closed with errors");
        }
    }
}
