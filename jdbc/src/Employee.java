import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.sql.Date;
import java.util.Locale;

/**
 * Created by Proinina Maria
 */
public class Employee {
    private int empno;
    private int deptno;
    private double sal;
    private String ename;
    private String job;
    private int mgr;
    private Date hiredate;
    private double comm;

    public Employee(int empno, int deptno, double sal, String ename, String job, int mgr, Date hiredate, double comm) {
        this.empno = empno;
        this.deptno = deptno;
        this.sal = sal;
        this.ename = ename;
        this.job = job;
        this.mgr = mgr;
        this.hiredate = hiredate;
        this.comm = comm;
    }

    public int getEmpno() {
        return empno;
    }

    public int getDeptno() {
        return deptno;
    }

    public double getSal() {
        return sal;
    }

    public String getEname() {
        return ename;
    }

    public String getJob() {
        return job;
    }

    public int getMgr() {
        return mgr;
    }

    public Date getHiredate() {
        return hiredate;
    }

    public double getComm() {
        return comm;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "empno=" + empno +
                ", deptno=" + deptno +
                ", sal=" + sal +
                ", ename='" + ename + '\'' +
                ", job='" + job + '\'' +
                ", mgr=" + mgr +
                ", hiredate=" + hiredate +
                ", comm=" + comm +
                '}';
    }
}
