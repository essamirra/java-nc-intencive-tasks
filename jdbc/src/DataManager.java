import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Proinina Maria
 */
public class DataManager {

    private final String DATABASE_URL = "jdbc:oracle:thin:@sql.edu-netcracker.com:1251:xe";
    private final String LOGIN = "unc17i_mpronina";
    private final String PASSWORD = "U5DqfAMm";
    private Connection connection = null;
    private PreparedStatement findEmploeeById = null;
    private PreparedStatement addEmploee = null;
    private PreparedStatement deleteEmploee = null;
    private PreparedStatement findEmploeeByJob = null;
    private PreparedStatement findEmploeeByDepartment = null;

    public void connectToDatabase() throws ClassNotFoundException, SQLException {
        if (connection != null) return;
        Locale.setDefault(Locale.ENGLISH);
        Class.forName("oracle.jdbc.driver.OracleDriver");
        connection = DriverManager.getConnection(DATABASE_URL, LOGIN, PASSWORD);
        createPreparedStatements();

    }

    private void createPreparedStatements() throws SQLException {
        if (connection == null) return;
        findEmploeeById = connection.prepareStatement("Select * from emp where empno = ?");
        deleteEmploee = connection.prepareStatement("Delete from emp where empno = ?");
        addEmploee = connection.prepareStatement("Insert into emp values (?, ?, ?, ?, ?, ?, ?, ?)");
        findEmploeeByJob = connection.prepareStatement("Select * from emp where job = ?");
        findEmploeeByDepartment = connection.prepareStatement("Select * from emp where deptno  = ?");
    }

    public Employee findEmploee(int id) throws SQLException {
        if (connection == null) return null;
        findEmploeeById.setInt(1, id);
        ResultSet resultSet = findEmploeeById.executeQuery();
        resultSet.next();
        return buildEmploeeFromResultSet(resultSet);
    }

    public List<Employee> findEmploeeByName(String job) throws SQLException {
        if (connection == null) return null;
        findEmploeeByJob.setString(1, job);
        ResultSet resultSet = findEmploeeByJob.executeQuery();
        List<Employee> result = new ArrayList<>();
        while (resultSet.next()){
           result.add(buildEmploeeFromResultSet(resultSet));
       }
        return result;
    }

    public List<Employee> findEmploeeByDept(int deptno) throws SQLException {
        if (connection == null) return null;
        findEmploeeByDepartment.setInt(1, deptno);
        ResultSet resultSet = findEmploeeByDepartment.executeQuery();
        List<Employee> result = new ArrayList<>();
        while (resultSet.next()){
            result.add(buildEmploeeFromResultSet(resultSet));
        }
        return result;
    }

    public void insertEmployee(Employee e) throws SQLException {
        if (connection == null) return;
        addEmploee.setInt(1, e.getEmpno());
        addEmploee.setString(2, e.getEname());
        addEmploee.setString(3, e.getJob());
        addEmploee.setInt(4, e.getMgr());
        addEmploee.setDate(5, e.getHiredate());
        addEmploee.setDouble(6, e.getSal());
        addEmploee.setDouble(7, e.getComm());
        addEmploee.setInt(8, e.getDeptno());
        addEmploee.executeUpdate();
    }

    private Employee buildEmploeeFromResultSet(ResultSet resultSet) throws SQLException {

        int id = resultSet.getInt("empno");
        int deptno = resultSet.getInt("deptno");
        String ename = resultSet.getString("ename");
        String job = resultSet.getString("job");
        double sal = resultSet.getDouble("sal");
        int mgr = resultSet.getInt("mgr");
        double comm = resultSet.getDouble("comm");
        Date hiredate = resultSet.getDate("hiredate");
        return new Employee(id, deptno, sal, ename, job, mgr, hiredate, comm);
    }

    public void deleteEmploee(int id) throws SQLException {
        deleteEmploee.setInt(1, id);
        deleteEmploee.executeUpdate();
    }



    public void closeConnection() throws SQLException {
        if (connection != null)
            connection.close();
    }

}
