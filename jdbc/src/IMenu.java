import java.util.Dictionary;
import java.util.Map;

/**
 * Created by Proinina Maria
 */
public interface IMenu {
    void showMessage(String message);
    void showOperationInput();
    void showMenu(Map<Integer, String> menu);
    void showStringParameterInput();
    void showIntParameterInput();
    void showEmployeeInput();
}
