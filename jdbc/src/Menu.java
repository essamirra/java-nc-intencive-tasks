import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.sql.Date;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Proinina Maria
 */
public class Menu implements IMenu {
    public static final String MESSAGE = "This is not an valid value. Input code again or input \"" + Presenter.EXIT_STRING +
            "\" to exit";
    public static final String SOMETHING_GONE_WRONG_PLEASE_TRY_AGAIN = "Something gone wrong, please try again";
    private Presenter presenter;
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(
            System.in));

    public Menu() {
        presenter = new Presenter(this);
    }

    @Override
    public void showMessage(String message) {
        System.out.println(message);
    }

    @Override
    public void showOperationInput() {
        String input = null;
        try {
            input = reader.readLine();
        } catch (IOException e) {
            showMessage(SOMETHING_GONE_WRONG_PLEASE_TRY_AGAIN);
            presenter.onMenuLoad();
        }
        needToExit(input);
        int result = -1;
        boolean flag = false;
        while (!flag) {
            try {
                result = Integer.parseInt(input);
                flag = true;
            } catch (NumberFormatException e) {
                showMessage("This is not an option code. Input code again or input \"" + Presenter.EXIT_STRING +
                        "\" to exit");
            }
        }
        presenter.onOperationInput(result);
    }

    private void needToExit(String input) {
        if (input.equals(Presenter.EXIT_STRING))
            presenter.exit();
    }

    @Override
    public void showMenu(Map<Integer, String> menu) {
        for (Map.Entry<Integer, String> e : menu.entrySet()) {
            showMessage(e.toString());
        }
    }

    public boolean showDialogMessage() {
        showMessage("Continue? Type yes for continue and no for exit to menu");
        String input = null;
        try {
            input = reader.readLine();
            return input.equals("yes");
        } catch (IOException e) {
            showMessage(SOMETHING_GONE_WRONG_PLEASE_TRY_AGAIN);
            return false;
        }
    }

    @Override
    public void showStringParameterInput() {
        String input = null;
        try {
            input = reader.readLine();
        } catch (IOException e) {
            showMessage(SOMETHING_GONE_WRONG_PLEASE_TRY_AGAIN);
            presenter.onMenuLoad();
        }

        if (showDialogMessage())
            presenter.onStringParameterInput(input);
        else
            presenter.onMenuLoad();

    }

    @Override
    public void showIntParameterInput() {
        String input = null;
        try {
            input = reader.readLine();
        } catch (IOException e) {
            showMessage(SOMETHING_GONE_WRONG_PLEASE_TRY_AGAIN);
            presenter.onMenuLoad();
        }
        needToExit(input);
        int result = -1;
        boolean flag = false;
        while (!flag) {
            try {
                result = Integer.parseInt(input);
                flag = true;
            } catch (NumberFormatException e) {
                showMessage(MESSAGE);
            }
        }
        if (showDialogMessage())
            presenter.onIntParameterInput(result);
        else presenter.onMenuLoad();
    }

    @Override
    public void showEmployeeInput() {
        String name = null;
        String job = null;
        int id = 0;
        int deptId = 0;
        double sal = 0;
        double comm = 0;
        Date hiredate = null;
        int mgr = 0;
        try {
            showMessage("Input name");
            name = reader.readLine();
            if (name.equals(Presenter.EXIT_STRING))
                presenter.exit();
            showMessage("Input job");
            job = reader.readLine();
            if (job.equals(Presenter.EXIT_STRING))
                presenter.exit();
            boolean flag = false;
            showMessage("Input employee id");
            while (!flag) {
                try {
                    String tmp = reader.readLine();
                    if (tmp.equals(Presenter.EXIT_STRING))
                        presenter.exit();
                    id = Integer.parseInt(tmp);
                    flag = true;
                } catch (NumberFormatException e) {
                    showMessage("This is not a valid id. Input id again or input \"" + Presenter.EXIT_STRING +
                            "\" to exit");
                }
            }
            flag = false;

            showMessage("Input department id");
            while (!flag) {
                try {
                    String tmp = reader.readLine();
                    if (tmp.equals(Presenter.EXIT_STRING))
                        presenter.exit();
                    deptId = Integer.parseInt(tmp);
                    flag = true;
                } catch (NumberFormatException e) {
                    showMessage("This is not a valid department id. Input id again or input \"" + Presenter.EXIT_STRING +
                            "\" to exit");
                }
            }
            flag = false;
            showMessage("Input manager id");
            while (!flag) {
                try {
                    String tmp = reader.readLine();
                    if (tmp.equals(Presenter.EXIT_STRING))
                        presenter.exit();
                    mgr = Integer.parseInt(tmp);
                    flag = true;
                } catch (NumberFormatException e) {
                    showMessage("This is not a valid manager id. Input id again or input \"" + Presenter.EXIT_STRING +
                            "\" to exit");
                }
            }
            flag = false;
            showMessage("Input salary");
            while (!flag) {
                try {
                    String tmp = reader.readLine();
                    if (tmp.equals(Presenter.EXIT_STRING))
                        presenter.exit();
                    sal = Double.parseDouble(tmp);
                    flag = true;
                } catch (NumberFormatException e) {
                    showMessage("This is not a valid salary. Input id again or input \"" + Presenter.EXIT_STRING +
                            "\" to exit");                }
            }
            flag = false;
            showMessage("Input commission");
            while (!flag) {
                try {
                    String tmp = reader.readLine();
                    if (tmp.equals(Presenter.EXIT_STRING))
                        presenter.exit();
                    else
                        comm = Double.parseDouble(tmp);
                    flag = true;
                } catch (NumberFormatException e) {
                    showMessage("This is not a valid commission. Input id again or input \"" + Presenter.EXIT_STRING +
                            "\" to exit");
                }
            }
            flag = false;
            showMessage("Input hiredate in format dd-MM-YY");
            while (!flag) {
                try {
                    String tmp = reader.readLine();
                    if (tmp.equals(Presenter.EXIT_STRING))
                        presenter.exit();

                    DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
                    java.util.Date temp = format.parse(tmp);
                    hiredate = new Date(temp.getTime());
                    flag = true;
                } catch (ParseException e) {
                    showMessage("This is not a valid date. Input id again or input \"" + Presenter.EXIT_STRING +
                            "\" to exit");
                }
            }

        } catch (IOException e) {
            showMessage(SOMETHING_GONE_WRONG_PLEASE_TRY_AGAIN);
            presenter.onMenuLoad();
        }
        if (showDialogMessage())
            presenter.onEmployeeInput(new Employee(id, deptId, sal, name, job, mgr, hiredate, comm));
        else
            presenter.onMenuLoad();
    }

    public void start() {
        presenter.onMenuLoad();
    }
}
